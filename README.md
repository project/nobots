# No Bots


This module blocks (well-behaved) search engine robots from crawling, indexing, or archiving your site by setting a
"X-Robots-Tag: noindex,nofollow,noarchive" HTTP header.

Enable the module, then using either settings.php file or state to activate the functionality.

Installation
---

Install this module like any other module. [See Drupal Documentation](https://drupal.org/documentation/install/modules-themes/modules-7)

Configuration
---

Use environment variables to block bots based on environment.
```php
if (getenv('ENV') === 'production') {
  $settings['nobots'] = TRUE;
}
```
Use scripts to block bots based on other configurable parameters.
```bash
drush state:set nobots 1
```

Or configure the value via the UI on the site settings page.
