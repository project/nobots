<?php

namespace Drupal\Tests\nobots\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test nobots header when enabled.
 *
 * @package Drupal\Tests\nobots\Unit
 * @group nobots
 */
class NobotsEnabledTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'nobots'];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('state')->set('nobots', TRUE);
    drupal_flush_all_caches();
  }

  /**
   * Test that the header exists and is correct.
   */
  public function testNobots() {
    $this->drupalGet('<front>');
    $header = $this->getSession()->getResponseHeader('x-robots-tag');
    $this->assertEquals('noindex,nofollow,noarchive', $header);
  }

}
